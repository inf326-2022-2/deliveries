import logging
import requests

from ariadne import QueryType
from ariadne import MutationType
from ariadne import ObjectType
from ariadne import make_executable_schema
from ariadne import load_schema_from_path

from ariadne.asgi import GraphQL

from graphql.type import GraphQLResolveInfo

from .dataloaders import TeamLoader

type_defs = load_schema_from_path("./app/schema.graphql")

query = QueryType()
mutation = MutationType()

Del = ObjectType("Delivery")

logging.basicConfig(level=logging.DEBUG,
                    format='%(asctime)s:%(levelname)s:%(name)s:%(message)s')


@query.field("getDelivery")
def resolve_get_player(obj, resolve_info: GraphQLResolveInfo, del_id):
    response = requests.get(f"http://Tarea_4/deliverys/{del_id}")

    if response.status_code == 200:
        return response.json()


schema = make_executable_schema(type_defs, query, mutation, Del)
app = GraphQL(schema, debug=True)