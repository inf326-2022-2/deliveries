FROM python:3.10

WORKDIR /code

COPY ./api_gateway /dockerfile
COPY ./Del_Service /dockerfile
COPY ./message_broker /dockerfile

RUN ls

RUN apt-get update && apt-get upgrade -y && \
    apt-get install -y nodejs \
    npm
RUN npm install
RUN npm i

CMD ["npm", "run", "start"]