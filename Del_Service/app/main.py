from ast import Del
import logging

from pymongo import MongoClient
from bson.errors import InvalidId
from bson.objectid import ObjectId
from fastapi import FastAPI
from fastapi import HTTPException
from pydantic import BaseModel

from .events import Emit


app = FastAPI()
mongodb_client = MongoClient("Tarea_4_Arqui", 27017)

emit_events = Emit()

logging.basicConfig(level=logging.INFO,
                    format='%(asctime)s:%(levelname)s:%(name)s:%(message)s')


class Delivery(BaseModel):
    del_id: int | None = None
    direccion: str
    producto: str
    fecha: str

    def __init__(self, **kargs):
        if "_id" in kargs:
            kargs["id"] = str(kargs["_id"])
        BaseModel.__init__(self, **kargs)


@app.get("/")
async def root():
    logging.info("👋 Hello world (end-point)!")
    return {"Hello": "World"}


@app.get("/deliverys", response_model=list[Delivery])
def delivery_all(team_id: str | None = None):
    filters = {}

    return [Delivery(**delivery) for delivery in mongodb_client.deliverys.find(filters)]


@app.get("/deliverys/{del_id}")
def deliverys_get(del_id: int):
    try:
        id = ObjectId(del_id)
        return Delivery(
            **mongodb_client.deliverys.find_one({"_id": del_id})
        )
    except (InvalidId, TypeError):
        raise HTTPException(status_code=404, detail="Delivery not found")


@app.put("/deliverys/{del_id}")
def deliverys_update(del_id: int, delivery: dict):
    try:
        del_id = ObjectId(del_id)
        mongodb_client.deliverys.update_one(
            {'_id': del_id}, {"$set": delivery})

        emit_events.send(del_id, "update", delivery.dict())

        return Delivery(
            **mongodb_client.deliverys.find_one({"_id": del_id})
        )

    except (InvalidId, TypeError):
        raise HTTPException(status_code=404, detail="Delivery not found")


@app.delete("/deliverys/{del_id}")
def deliverys_delete(del_id: int):
    try:
        del_id = ObjectId(del_id)
        delivery = Delivery(
            **mongodb_client.deliverys.find_one({"_id": del_id})
        )
    except (InvalidId, TypeError):
        raise HTTPException(status_code=404, detail="Delivery not found")

    mongodb_client.deliverys.delete_one(
        {"_id": ObjectId(del_id)}
    )

    emit_events.send(del_id, "delete", Delivery.dict())

    return delivery


@app.post("/deliverys")
def deliverys_create(delivery: Delivery):
    inserted_id = mongodb_client.deliverys.insert_one(
        delivery.dict()
    ).inserted_id

    new_del = Delivery(
        **mongodb_client.deliverys.find_one(
            {"_id": ObjectId(inserted_id)}
        )
    )

    emit_events.send(inserted_id, "create", new_del.dict())

    logging.info(f"✨ New player created: {new_del}")

    return new_del